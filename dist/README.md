# README #

Este es una estructura base desarrollada para maquetas de themas en [www.yiiframework.com](http://www.yiiframework.com)

### Instalación ###

En necesario tener un archivo .htaccess en la raíz del proyecto para las url limpias
 
```
#!bash

RewriteEngine on

# if a directory or a file exists, use it directly
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# otherwise forward it to index.php
RewriteRule . index.php

```

En algunos casos como por ejemplo e el servidor de desarrollo es posible que necesitemos agregar
el RewriteBase por ejemplo para el servidor de desarrollo de mentes web hay que subir este .htaccess

 
```
#!bash

RewriteEngine on
RewriteBase /~menteswe/myproyectname/

# if a directory or a file exists, use it directly
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# otherwise forward it to index.php
RewriteRule . index.php

```
