/*!
  File name: add.js
  Description: Additional functions
*/

/*global $, jQuery, document, console, alert*/


// Autocomplete
var substringMatcher = function (strs) {
    "use strict";
    return function findMatches(q, cb) {
        var matches, substrRegex;
        // an array that will be populated with substring matches
        matches = [];
        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, "i");
        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        /*jslint unparam: true*/
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({value: str});
            }
        });
        /*jslint unparam: false*/
        cb(matches);
    };
};

var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

$("#auto-search .typeahead").typeahead({
    hint: true,
    highlight: true,
    minLength: 1
}, {
    name: "states",
    displayKey: "value",
    source: substringMatcher(states)
});
// Autocomplete




document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    console.log("add scripts");


    // File input
    $("#file-content").fileinput({
        uploadUrl: "' . $url1 . '",
        uploadAsync: false,
        minFileCount: 1,
        maxFileCount: 10,
        overwriteInitial: false
    });
    $("#file-promotion").fileinput({
        uploadUrl: "' . $url1 . '",
        uploadAsync: false,
        minFileCount: 1,
        maxFileCount: 10,
        overwriteInitial: false
    });

    $("#file-cover").fileinput({
        uploadUrl: "' . $url1 . '",
        uploadAsync: false,
        minFileCount: 1,
        maxFileCount: 10,
        overwriteInitial: false
    });

    $("#file-perfil").fileinput({
        uploadUrl: "' . $url1 . '",
        uploadAsync: false,
        minFileCount: 1,
        maxFileCount: 10,
        overwriteInitial: false
    });

    // File input


}, false);
