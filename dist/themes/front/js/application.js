/*!
  Author: Stive Zambrano
  Creation date: March of 2015
  Project name: wepiku
  File name: application.js
  Description: Global functions
*/


var
    tMs = TweenMax.set,
    tMt = TweenMax.to,
    tLs = TweenLite.set,
    tLt = TweenLite.to,
    container,
    Masonry,
    msnry,
    imagesLoaded;






function loadFn() {
    "use strict";
    tLt(".wepiku-item", 0.6, {
        className: "+=active",
        ease: Cubic.easeOut
    });
    if ($(window).innerHeight() < 650) {
        $(".nav-main").addClass("nav-static");
    }

    tLt(".poll-banner-icon1, .poll-banner-icon2", 0.1, {
        className: "+=active"
    });


    tLt(".poll-banner-step1", 0.1, {
        className: "+=active",
        delay: 0.8
    });

    tLt(".poll-banner-step2", 0.1, {
        className: "+=active",
        delay: 1
    });

    tLt(".poll-banner-step3", 0.1, {
        className: "+=active",
        delay: 1.2
    });

}





$(window).on("load resize", function () {
    "use strict";
    var
        // center = map.getCenter(),
        windowH = $(window).innerHeight();


    $("#map-container").height(windowH - 54);
    $("#bh-sl-map").height(windowH - 54);
    // google.maps.event.trigger(map, "resize");
    // map.setCenter(center);


});


// hasClass
function hasClass(elem, className) {
    "use strict";
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}


// toggleClass
function toggleClass(elem, className) {
    "use strict";
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0) {
            newClass = newClass.replace(" " + className + " ", " ");
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}





// if (document.querySelector(".col-main").className.match(/\bgrid-section\b/)) {
document.querySelector(".btn-sub-nav").onclick = function () {
    "use strict";
    var wrapper = document.querySelector(".sub-nav");
    toggleClass(wrapper, "active");
};
// }


document.querySelector(".call-stores").onclick = function () {
    "use strict";
    var wrapper = document.querySelector("body");
    toggleClass(wrapper, "app-active");
    $(".close-pro").trigger("click");
};


document.querySelector(".close-app").onclick = function () {
    "use strict";
    var wrapper = document.querySelector("body");
    toggleClass(wrapper, "app-active");
};



if (document.querySelector(".col-main").className.match(/\bbrand-section\b/)) {
    document.querySelector(".btn-favorite").onclick = function () {
        "use strict";
        var
            wrapper = document.querySelector(".brand-like");
        toggleClass(wrapper, "active");
    };
}





$(document).on("click", "btn-test", function () {
    "use strict";
    var sedes = [
      /*Marker*/
      ['<div class="row tool-map">'+
        '<div class="col-md-6"><img src="img/map-img.jpg"></div>'+
        '<div class="col-md-6 col-info-contact">'+
          '<h4>Lorem ipsum</h4><div class="clearfix"></div>'+
          '<p>Cll. 999 No. 999-999</p><div class="clearfix"></div>'+
          '<p>Tel: 123 4567</p><div class="clearfix"></div>'+
          '<p>Bogotá - Colombia</p><div class="clearfix"></div>'+
        '</div>'+
      '</div>', 4.598056, -74.075833],
      /*Fin marker*/
    ],
    map = new google.maps.Map(document.getElementById("map"), {
      //scrollwheel: false,
      zoom: 14,
      center: new google.maps.LatLng(4.598056, -74.075833)
    }),
    image = {
      url: "img/marker-default.png",
      size: new google.maps.Size(48, 42),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(16, 42)
    },
    infowindow = new google.maps.InfoWindow(), marker, i;
    for (i = 0; i < sedes.length; i++) {
      marker = new google.maps.Marker({
        icon: image,
        position: new google.maps.LatLng(sedes[i][1], sedes[i][2]),
        map: map,
        zIndex: sedes[3]
      });
      google.maps.event.addListener(marker, "click", (function(marker, i) {
        return function() {
          infowindow.setContent(sedes[i][0]);
          infowindow.open(map, marker);
        }})(marker, i));
    };
    document.addEventListener("DOMContentLoaded", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });
});






if (document.querySelector(".col-main").className.match(/\bgrid-section\b/)) {
    container = document.querySelector(".wepiku-grid");
    imagesLoaded(container, function () {
        "use strict";
        msnry = new Masonry(container, {
            columnWidth: 375,
            itemSelector: ".wepiku-item"
        });
    });
}



if (document.querySelector(".section-id").className.match(/\bsection-plan\b/) || document.querySelector(".section-id").className.match(/\bsection-encuesta\b/)) {
    $(".sub-nav").hide();
}


if (document.querySelector(".section-id").className.match(/\bsection-new-mark\b/)) {
    $(".sub-nav, .top-search").hide();
}






window.onload = function () {
    "use strict";

    tLt("#preload", 0.8, {
        css: {
            autoAlpha: 0,
            scale: 2
        },
        delay: 2,
        ease: Cubic.easeOut,
        onComplete: loadFn
    });

    if ($(".main-section").data("section") === "map") {
        $("#map-container").storeLocator();
    }

};




$(document).scroll(function () {
    "use strict";
    $(".brand-page-head").css({
        marginTop: (-$(this).scrollTop() / 10)
    });
    $(".brand-head-fx").css({
        marginTop: (-$(this).scrollTop())
    });
});










function multiSelect(select) {
    "use strict";
    var
        option = select.options[select.selectedIndex],
        ul = select.parentNode.getElementsByTagName("ul")[0],
        choices = ul.getElementsByTagName("input"),
        li = document.createElement("li"),
        input = document.createElement("input"),
        text = document.createTextNode(option.firstChild.data),
        i;

    for (i = 0; i < choices.length; i = i + 1) {
        if (choices[i].value === option.value) {
            return;
        }
    }

    // var
    //     li = document.createElement("li"),
    //     input = document.createElement("input"),
    //     text = document.createTextNode(option.firstChild.data);

    input.type = "hidden";
    input.name = "multi[]";
    input.value = option.value;

    li.appendChild(input);
    li.appendChild(text);
    li.setAttribute("onclick", "this.parentNode.removeChild(this);");

    ul.appendChild(li);
}









document.addEventListener("DOMContentLoaded", function () {
    "use strict";

    var
        // wH = $(window).innerHeight(),
        // wW = $(window).innerWidth(),
        Fn = Function,
        // tMs = TweenMax.set,
        tMt = TweenMax.to,
        videoOne = $(".video-content"),
        videoTwo = $(".video-promotion"),
        btnVideoOne = $(".btn-add-video-content"),
        btnVideoTwo = $(".btn-add-video-promotion");

    // Misc
    if (new Fn("/*@cc_on return document.documentMode===10@*/")()) {
        document.documentElement.className += " ie10";
    }

    $(".over-bw").hover(function () {
        tMt($(this).find(".icon-bw"), 0.6, {marginTop: -12, ease: Cubic.easeOut});
    }, function () {
        tMt($(this).find(".icon-bw"), 0.6, {marginTop: 0, ease: Cubic.easeOut});
    });

    $(".bt-bw").on("click", function () {
        tMt(".con-bw", 0.6, {autoAlpha: 0, ease: Cubic.easeOut});
    });


    $("html, body").keyup(function (e) {
        if (e.keyCode === 27) {
            $(".modal-item").modal("hide");
            $(".modal-stores").modal("hide");
            $("body").removeClass("app-active");
        }
    });


    // Custom check
    $(".check-active").iCheck({
        checkboxClass: "icheckbox_minimal",
        radioClass: "iradio_minimal",
        increaseArea: "20%" // optional
    });

    $(".check-active-content").on("ifChecked", function () {
        $(".item-poll-content").addClass("active");
        // alert(event.type + " callback");
    }).on("ifUnchecked", function () {
        $(".item-poll-content").removeClass("active");
        // alert(event.type + " callback");
    });

    $(".check-active-promotion").on("ifChecked", function () {
        $(".item-poll-promotion").addClass("active");
        // alert(event.type + " callback");
    }).on("ifUnchecked", function () {
        $(".item-poll-promotion").removeClass("active");
        // alert(event.type + " callback");
    });



    // Dates

    if (document.querySelector(".section-id").className.match(/\bsection-plan\b/) || document.querySelector(".section-id").className.match(/\bsection-encuesta\b/)) {
        $("#date-poll-content").dateRangePicker({
            alwaysOpen: true,
            container: ".item-poll-date-content",
            format: "DD MMMM",
            language: "es",
            inline: true,
            separator: "",
            // shortcuts: null,
            showShortcuts: false,
            startOfWeek: "monday",
            stickyMonths: true
        });
    }

    if (document.querySelector(".section-id").className.match(/\bsection-plan\b/)) {
        $("#date-poll-promotion").dateRangePicker({
            alwaysOpen: true,
            container: ".item-poll-date-promotion",
            format: "DD MMMM",
            language: "es",
            inline: true,
            separator: "",
            // shortcuts: null,
            showShortcuts: false,
            startOfWeek: "monday",
            stickyMonths: true
        });
    }




    $(".item-poll-box-text").on("click", function () {
        $(this).next(".item-poll-box-edit").addClass("active");
    });



    // Add videos
    btnVideoOne.click(function () {
        $(this).siblings(".row-videos").find(".list-videos").append("<li class='see-video'>Youtube - " + videoOne.val() + "</li>");
        videoOne.val("");
    });
    btnVideoTwo.click(function () {
        $(this).siblings(".row-videos").find(".list-videos").append("<li class='see-video'>Youtube - " + videoTwo.val() + "</li>");
        videoTwo.val("");
    });









    $(".btn-count-up").click(function () {
        var counter = +$(this).siblings(".counter-number").find(".number").text() + 1;

        $(this).siblings(".counter-number").find(".number").html(counter);

        // $(this).siblings(".counter-number").find(".number").stop().animate({top: "-100%"}, 200, function () {
        //  $(this).css({opacity: 0, top: "100%"}).stop().animate({opacity: 1, top: 0}, 50, function () {
        //    $(this).html(counter);
        //    });
        // });
    });


    $(".btn-count-down").click(function () {
        var counter = +$(this).siblings(".counter-number").find(".number").text() - 1;

        if (counter >= 1) {
            $(this).siblings(".counter-number").find(".number").html(counter);
        }

        // if (counter >= 1) {
        //  $(this).siblings(".counter-number").find(".number").stop().animate({top: "100%"}, 200, function () {
        //    $(this).css({opacity: 0, top: "-100%"}).stop().animate({opacity: 1, top: 0}, 50, function () {
        //      $(this).html(counter);
        //    });
        //  });
        // }
    });




    // $(".go-to").on("click", function () {
    //     tMt($("html, body"), 1.2, {scrollTop: $($(this).attr("data-rel")).offset().top, ease: Cubic.easeOut});
    // });




    if ($(".main-section").data("section") === "map") {
        $(".nav3").addClass("active");
        document.querySelector(".map-filter").onclick = function () {
            var wrapper = document.querySelector(".categories-filter");
            toggleClass(wrapper, "active");
        };
    }



    if ($(".main-section").data("section") === "points") {
        $(".nav1").addClass("active");
    }




    $(".perfil-info-close").on("click", function () {
        "use strict";
        $(this).parent().remove();
    });




    $(".btn-add-fburl").on("click", function () {
        "use strict";
        var fburl = $(".input-fburl").val();
        $(".row-fburl").append("<div class='col-xs-12'><div class='row'><div class='col-xs-11'><p>" + fburl + "</p></div><div class='col-xs-1'><button class='btn btn-remove-fburl text-center' type='button'><span></span></button></div></div><span class='br-fburl'></span></div>");
        $(".input-fburl").val("");
    });





    $(window).scroll(function () {
        var pSc = $(window).scrollTop();
        if (pSc >= 50) {
            console.log(">=50");
        } else {
            console.log("<50");
        }
    });



  /*Add*/
    if ($(".footer-ahorranito").size() > 0) {
        $(".footer-ahorranito").ahorranito();
    }
}, false);


$(document).on("click", ".see-video", function () {
    "use strict";
    $(this).hide();
});


$(document).on("click", ".btn-year", function () {
    "use strict";
    $(".btn-year").removeClass("active");
    $(this).addClass("active");
});


$(document).on("click", ".btn-remove", function () {
    "use strict";
    $(this).parents().eq(1).remove();
});


$(document).on("click", ".btn-remove-fburl", function () {
    "use strict";
    $(this).parents().eq(2).remove();
});


$("[data-call='main-slider']").each(function () {
    "use strict";
    $(this).bxSlider();
});
