<!--					<section>-->

<div class="col-xs-12 col-sm-7 col-grid col-main grid-section section-id">
        <div class="top-search clearfix">
                <form action="#" class="top-form pull-left clearfix">
                        <button class="btn-search" type="button"></button>
                        <div id="auto-search">
                        <input class="typeahead" type="text">
                        </div>
                </form>
                <div class="dropdown pull-right">
                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    <span class="sub-list-icon"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                  </ul>
                </div>
        </div>
        <div class="wepiku-grid">

                <!-- Item -->
                <?php foreach ($data_hotdeals_json['data'] as $campania): ?>
                <div class="wepiku-item">
                        <div class="<?php echo 'item item-type'.$campania['tipo_id']; ?>">   
                            <?php if($campania['descuento'] != "" and ($campania['tipo_id'] == 1 || $campania['tipo_id'] == 4)): ?>
                                <div class="discount" data-toggle="modal" data-target="#modal-product">
                                        <?php echo $campania['descuento']; ?>
                                </div>
                            <?php endif; ?>
                            <?php if($campania['tipo_id'] == 3): ?>
                                <div class="item-type3-bg" style="background-image: url(<?php echo stripslashes($campania['enlace']); ?>);">
                                </div>                                    
                            <?php else: ?>
                                <?php echo CHtml::image(stripslashes($campania['enlace']), "campania",  array('class'=>'img-main','data-toggle'=>'modal','data-target'=>'#modal-product')); ?>                                                               
                                <?php //echo CHtml::link($image, array('campania/detalle')); ?>
                            <?php endif; ?>    
                                <div class="caption">
                                    <?php if($campania['tipo_id'] == 1): ?>
                                        <div class="shadow"></div>
                                        <div class="logo">
                                                <?php $image = CHtml::image(Yii::app()->theme->baseUrl."/img/logo-mini1.png", "logo"); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                <?php echo CHtml::link($image, $this->createUrl('marca')); ?>
                                        </div>
                                        <h2 class="clearfix" data-toggle="modal" data-target="#modal-product"><?php echo $campania['nombre']; ?></h2>
                                    <?php elseif($campania['tipo_id'] == 2): ?>    
                                        <div class="table">
                                                <div class="table-cell">
                                                        <div class="row row-table">
                                                                <div class="col-xs-4 col-cell">
                                                                        <div class="logo">
                                                                            <?php $image = CHtml::image(Yii::app()->theme->baseUrl."/img/logo-mini2.png", "logo"); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                                            <?php echo CHtml::link($image, $this->createUrl('marca')); ?>                                                                                
                                                                        </div>
                                                                </div>
                                                                <div class="col-xs-8 col-cell text-right" id="campania_padding" data-toggle="modal" data-target="#modal-product">
                                                                        <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                    <?php elseif($campania['tipo_id'] == 3): ?> 
                                        <?php //foreach($campania['campaniaEncuestas']['encuesta']['preguntas'] as $preguntas): ?>
                                            <p><?php echo $campania['nombre']; ?>
                                                <div class="icon"></div>
                                            </p>
                                            <h2 class="clearfix">
                                                <?php echo $campania['texto_oferta']; ?>
                                            </h2>
                                            <div class="logo">
                                                    <a href="<?php echo $this->createUrl('marca')?>">
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-page.png" alt="">
                                                    </a>
                                            </div>   
                                        <?php //endforeach; ?>
                                    <?php endif; ?>
                                </div>
                                <?php if($campania['tipo_id'] == 1): ?>
                                <div class="item-date">
                                        <h5 class="text-center"><?php echo $campania['texto_oferta']; ?></h5>
                                </div>                                
                                <?php endif; ?>
                        </div>                                
                </div>
                <?php endforeach; ?>

                <!--/ Item -->
        </div>
</div>

<div class="col-xs-12 col-sm-2 col-sub-nav">
    <div class="sub-nav active">
            <button class="btn-sub-nav" type="button">
                    <span class="btn-arrow"></span>
                    <span class="btn-icon"></span>                                                                
            </button>
            <div class="sub-nav-list">
            <?php foreach ($data_newsfeed_json['data'] as $newfeed): ?>   
                    <?php if($newfeed['campania']['tipo_id'] == 2): ?>
                        <div class="<?php echo 'item-sub-list item-post'.$newfeed['campania']['tipo_id']; ?>">
                    <?php else: ?>
                        <div class="<?php echo 'item-sub-list item-post2'; ?>">                            
                    <?php endif; ?>
                            <div class="row">
                                    <div class="col-xs-12 img-users">
                                        <?php $num_usuarios = count($newfeed['usuario']); ?>
                                        <?php foreach ($newfeed['usuario'] as $usuario_amigo): ?>
                                            <?php echo CHtml::image($usuario_amigo['img'], "logo",  array("class"=>"img-circle", "width"=>"30px")); ?>
                                        <?php endforeach; ?>
                                        <h4>
                                        <?php $i=0; ?>
                                        <?php foreach ($newfeed['usuario'] as $usuario_amigo): ?>
                                            <?php $i++; ?>
                                            <?php if($num_usuarios > $i): ?>
                                                    <?php echo '<strong>'.$usuario_amigo['name'].' '.$usuario_amigo['lastname'].',</strong>'; ?>
                                            <?php else: ?>
                                                    <?php echo '<strong>'.$usuario_amigo['name'].' '.$usuario_amigo['lastname'].'</strong>'; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <br/>    
                                            <?php switch ($newfeed['puntos']['id']):
                                                case 1:
                                                    if($num_usuarios == 1):
                                                        echo 'revisó la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    elseif($num_usuarios > 1):
                                                        echo 'revisaron la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    endif;
                                                    break;
                                                case 2:
                                                    if($num_usuarios == 1):
                                                        echo 'hizo pik en la promoción de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    elseif($num_usuarios > 1):
                                                        echo 'hicieron pik en la promoción de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    endif;
                                                    break; 
                                                case 5:
                                                    if($num_usuarios == 1):
                                                        echo 'respondió la encuesta de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    elseif($num_usuarios > 1):
                                                        echo 'respondieron la encuesta de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    endif;
                                                    break;      
                                                case 7:
                                                    if($num_usuarios == 1):
                                                        echo 'quiere compartir la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    elseif($num_usuarios > 1):
                                                        echo 'quieren compartir la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    endif;
                                                    break; 
                                                case 9:
                                                    if($num_usuarios == 1):
                                                        echo 'comentó en la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    elseif($num_usuarios > 1):
                                                        echo 'comentaron en la campaña de '.$data_marca_json[$newfeed['campania']['marca_id']];    
                                                    endif;
                                                    break;  
                                                case 10:
                                                        echo $data_marca_json[$newfeed['campania']['marca_id']].' te recomienda ';    
                                                    break;                                                      
                                            endswitch; ?>
                                        </h4>
                                    </div>
                                    <div class="col-xs-12 pik-users">
                                            <a class="brand-link" href="#">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini7.png" alt="">
                                            </a>
                                            <a class="brand-promo" href="#">
                                                <?php if($newfeed['campania']['tipo_id'] ==1 || $newfeed['campania']['tipo_id'] == 4): ?>
                                                    <div class="discount">
                                                            <?php echo $newfeed['campania']['descuento']; ?>
                                                    </div>
                                                <?php endif; ?>                                                
                                                    <div class="shadow"></div>
                                                    <?php echo CHtml::image(stripslashes($newfeed['campania']['enlace']), "newfeed"); ?>
                                            </a>
                                    </div>
                                    <div class="col-xs-12 name-promo">
                                            <h5>
                                                    <a href="#"><?php echo strtoupper($newfeed['campania']['nombre']); ?><span></span></a>
                                            </h5>
                                    </div>
                            </div>
                    </div>
                <?php endforeach; ?>
            </div>
    </div>
</div>


<!--
                                        <h1 class="main-title clearfix">Home</h1>
                                        <i class="fa fa-camera-retro fa-5x"></i>
                                        <a class="link" href="<?php #echo $this->createUrl('section')?>">Go section</a>
-->


<!--					</section>-->
