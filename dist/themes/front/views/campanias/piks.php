<!--					<section>-->

    <div class="col-xs-12 col-sm-7 col-grid col-main grid-section section-id">
            <div class="top-search clearfix">
                    <form action="#" class="top-form pull-left clearfix">
                            <button class="btn-search" type="button"></button>
                            <div id="auto-search">
                            <input class="typeahead" type="text">
                            </div>
                    </form>
                    <div class="dropdown pull-right">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        <span class="sub-list-icon"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                      </ul>
                    </div>
            </div>
            <div class="wepiku-grid">

                    <!-- Item -->
                    <?php foreach ($data_hotdeals_json['data'] as $campania): ?>
                    <div class="wepiku-item">
                            <div class="<?php echo 'item item-type'.$campania['tipo_id']; ?>">   
                                <?php if($campania['descuento'] != "" and ($campania['tipo_id'] == 1 || $campania['tipo_id'] == 4)): ?>
                                    <div class="discount" data-toggle="modal" data-target="#modal-product">
                                            <?php echo $campania['descuento']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if($campania['tipo_id'] == 3): ?>
                                    <div class="item-type3-bg" style="background-image: url(<?php echo stripslashes($campania['enlace']); ?>);">
                                    </div>                                    
                                <?php else: ?>
                                    <?php echo CHtml::image(stripslashes($campania['enlace']), "campania",  array('class'=>'img-main','data-toggle'=>'modal','data-target'=>'#modal-product')); ?>                                                               
                                    <?php //echo CHtml::link($image, array('campania/detalle')); ?>
                                <?php endif; ?>    
                                    <div class="caption">
                                        <?php if($campania['tipo_id'] == 1): ?>
                                            <div class="shadow"></div>
                                            <div class="logo">
                                                    <?php $image = CHtml::image(Yii::app()->theme->baseUrl."/img/logo-mini1.png", "logo"); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                    <?php echo CHtml::link($image, $this->createUrl('marca')); ?>
                                            </div>
                                            <h2 class="clearfix" data-toggle="modal" data-target="#modal-product"><?php echo $campania['nombre']; ?></h2>
                                        <?php elseif($campania['tipo_id'] == 2): ?>    
                                            <div class="table">
                                                    <div class="table-cell">
                                                            <div class="row row-table">
                                                                    <div class="col-xs-4 col-cell">
                                                                            <div class="logo">
                                                                                <?php $image = CHtml::image(Yii::app()->theme->baseUrl."/img/logo-mini2.png", "logo"); ?> <!--,  array("width"=>"40px" ,"height"=>"40px")-->
                                                                                <?php echo CHtml::link($image, $this->createUrl('marca')); ?>                                                                                
                                                                            </div>
                                                                    </div>
                                                                    <div class="col-xs-8 col-cell text-right" id="campania_padding" data-toggle="modal" data-target="#modal-product">
                                                                            <h2 class="clearfix"><?php echo $campania['nombre']; ?></h2>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                        <?php elseif($campania['tipo_id'] == 3): ?> 
                                            <?php //foreach($campania['campaniaEncuestas']['encuesta']['preguntas'] as $preguntas): ?>
                                                <p><?php echo $campania['nombre']; ?>
                                                    <div class="icon"></div>
                                                </p>
                                                <h2 class="clearfix">
                                                    <?php echo $campania['texto_oferta']; ?>
                                                </h2>
                                                <div class="logo">
                                                        <a href="<?php echo $this->createUrl('marca')?>">
                                                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-page.png" alt="">
                                                        </a>
                                                </div>   
                                            <?php //endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($campania['tipo_id'] == 1): ?>
                                    <div class="item-date">
                                            <h5 class="text-center"><?php echo $campania['texto_oferta']; ?></h5>
                                    </div>                                
                                    <?php endif; ?>
                            </div>                                
                    </div>
                    <?php endforeach; ?>
                    
                    <!--/ Item -->
            </div>
    </div>

    <div class="col-xs-12 col-sm-2 col-sub-nav">


            <div class="sub-nav active">
                    <button class="btn-sub-nav" type="button">
                            <span class="btn-arrow"></span>
                            <span class="btn-icon"></span>                                                                
                    </button>
                    <div class="sub-nav-list">

                            <!-- Item type1 -->
                            <div class="item-sub-list item-post1">
                                    <div class="row">
                                            <div class="col-xs-12 img-users">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img1.jpg" alt="" width="30">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img2.jpg" alt="" width="30">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img3.jpg" alt="" width="30">
                                            </div>
                                            <div class="col-xs-12 name-users">
                                                    <h4><strong>Carlos M, Pancho L, Rafa J </strong> acaban de hacer Pik</h4>
                                            </div>
                                            <div class="col-xs-12 pik-users">
                                                    <a href="#">
                                                            <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini2.png" alt="">
                                                    </a>
                                                    <h5>
                                                            <a href="#">
                                                                    <strong>Sector 9 Skateboards</strong>
                                                                    Calle 85# 00 - 00
                                                                    <span></span>
                                                            </a>
                                                    </h5>
                                            </div>
                                    </div>
                            </div>
                            <!--/ Item type1 -->

                            <!-- Item type2 -->
                            <div class="item-sub-list item-post2">
                                    <div class="row">
                                            <div class="col-xs-12 img-users">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img4.jpg" alt="" width="30">
                                                    <h4><strong>Clarissa Rodríguez </strong> compartió una publicación</h4>
                                            </div>
                                            <div class="col-xs-12 pik-users">
                                                    <a class="brand-link" href="#">
                                                            <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini7.png" alt="">
                                                    </a>
                                                    <a class="brand-promo" href="#">
                                                            <div class="discount">
                                                                    60%
                                                            </div>
                                                            <div class="shadow"></div>
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img7.jpg" alt="">
                                                    </a>
                                            </div>
                                            <div class="col-xs-12 name-promo">
                                                    <h5>
                                                            <a href="#">
                                                                    CHAQUETA DE JEAN ROXY CON BORDADOS DE COLORES
                                                            </a>
                                                    </h5>
                                            </div>
                                    </div>
                            </div>
                            <!--/ Item type2 -->

                            <!-- Item type2 -->
                            <div class="item-sub-list item-post2">
                                    <div class="row">
                                            <div class="col-xs-12 img-users">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img5.jpg" alt="" width="30">
                                                    <h4><strong>Carlos Cordero </strong> compartió una publicación</h4>
                                            </div>
                                            <div class="col-xs-12 pik-users">
                                                    <a class="brand-link" href="#">
                                                            <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini3.png" alt="">
                                                    </a>
                                                    <a class="brand-promo" href="#">
                                                            <div class="discount">
                                                                    10%
                                                            </div>
                                                            <div class="shadow"></div>
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img3.jpg" alt="">
                                                    </a>
                                            </div>
                                            <div class="col-xs-12 name-promo">
                                                    <h5>
                                                            <a href="#">
                                                                    COLECCIÓN 2015 DOWNHILL TIME ARBOR SKATEBOARDS
                                                            </a>
                                                    </h5>
                                            </div>
                                    </div>
                            </div>
                            <!--/ Item type2 -->

                            <!-- Item type2 -->
                            <div class="item-sub-list item-post2">
                                    <div class="row">
                                            <div class="col-xs-12 img-users">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img6.jpg" alt="" width="30">
                                                    <h4><strong>Lorem Ipsum </strong> compartió una publicación</h4>
                                            </div>
                                            <div class="col-xs-12 pik-users">
                                                    <a class="brand-link" href="#">
                                                            <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini8.png" alt="">
                                                    </a>
                                                    <a class="brand-promo" href="#">
                                                            <div class="discount">
                                                                    10%
                                                            </div>
                                                            <div class="shadow"></div>
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img9.jpg" alt="">
                                                    </a>
                                            </div>
                                            <div class="col-xs-12 name-promo">
                                                    <h5>
                                                            <a href="#">
                                                                    AFTER CLASS 2015 SPRING SEASON
                                                            </a>
                                                    </h5>
                                            </div>
                                    </div>
                            </div>
                            <!--/ Item type2 -->

                            <!-- Item type3 -->
                            <div class="item-sub-list item-post3">
                                    <div class="row">
                                            <div class="col-xs-12 img-users">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img1.jpg" alt="" width="30">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img2.jpg" alt="" width="30">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img3.jpg" alt="" width="30">
                                            </div>
                                            <div class="col-xs-12 name-users">
                                                    <h4><strong>Carlos M, Pancho L, Rafa J </strong> acaban de hacer Pik</h4>
                                            </div>
                                            <div class="col-xs-12 pik-users">
                                                    <a class="brand-link" href="#">
                                                            <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini5.png" alt="">
                                                    </a>
                                                    <a class="brand-promo" href="#">
                                                            <div class="discount">
                                                                    45%
                                                            </div>
                                                            <div class="shadow"></div>
                                                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img5.jpg" alt="">
                                                    </a>
                                            </div>
                                            <div class="col-xs-12 name-promo">
                                                    <h5>
                                                            <a href="#">
                                                                    VANS CLASSIC VARIOS COLORES
                                                            </a>
                                                    </h5>
                                            </div>
                                    </div>
                            </div>
                            <!--/ Item type3 -->

                    </div>
            </div>
    </div>


<!--
                                        <h1 class="main-title clearfix">Home</h1>
                                        <i class="fa fa-camera-retro fa-5x"></i>
                                        <a class="link" href="<?php #echo $this->createUrl('section')?>">Go section</a>
-->


<!--					</section>-->
