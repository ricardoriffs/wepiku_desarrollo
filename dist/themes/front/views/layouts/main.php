<?php Yii::app()->clientScript->registerCoreScript('jquery-front');?>
<?php //session_start(); ?>
<!DOCTYPE html>
	<!--[if lt IE 8]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="es"> <![endif]-->
	<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9 ie8" lang="es"> <![endif]-->
	<!--[if IE 9]> <html class="no-js lt-ie10 ie9" lang="es"> <![endif]-->
	<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
	<head>
		<title>Wepiku</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="date" content="<?php echo date("Y"); ?>" />
		<meta name="author" content="diseño web: imaginamos.com" />
		<meta name="robots" content="All" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
		<meta name="viewport" content="width=1320, maximum-scale=2" />
		<meta name="Keywords" content="palabras clave" lang="es" />
		<meta name="Description" content="texto empresarial" lang="es" />
		<link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />

		<!--Styles after front-end-->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/add/styles.css" />

		<!-- <script async src="http://modernizr.com/downloads/modernizr-latest.js" type="text/javascript"></script> -->
		<script async type="text/javascript">var baseUrl = "<?php echo Yii::app()->theme->baseUrl; ?>";</script>

		<?php echo $this->builtHeader()?>
	</head>
	<body>
		<div id="preload"><div class="preload-logo"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div></div>
		<div class="con-bw"><div class="info-bw"><div class="head-bw clearfix"><div class="logo-bw left"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png"></div><div class="tx-bw left"><p>Oops!... Lo sentimos, este sitio se ha desarrollado para navegadores modernos con el fin de mejorar tu experiencia.</p><p>Para que lo puedas disfrutar es necesario actualizar tu navegador o simplemente descargar e instalar uno mejor.</p></div></div><div class="con-icon-bw left"><a href="https://www.google.com/intl/es/chrome/browser/?hl=es" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-1.png"></div><p>Chrome</p></a></div><div class="con-icon-bw left"><a href="http://www.mozilla.org/es-ES/firefox/new/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-2.png"></div><p>Firefox</p></a></div><div class="con-icon-bw left"><a href="http://www.opera.com/es-419/computer/" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-3.png"></div><p>Opera</p></a></div><div class="con-icon-bw left"><a href="http://support.apple.com/kb/DL1531?viewlocale=es-ES" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-4.png"></div><p>Safari</p></a></div><div class="con-icon-bw left"><a href="http://windows.microsoft.com/es-es/internet-explorer/download-ie" target="-blank" class="over-bw"><div class="icon-bw"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/bw-5.png"></div><p>Explorer</p></a></div><div class="con-bt-bw left clearfix"><a class="bt-bw"></a></div></div></div>



		<!-- <div class="line-h"></div>
		<div class="line-v"></div>
		<div class="line-ml"></div>
		<div class="line-mr"></div> -->



<!--
		<div id="main-wrapper">
			<div class="animated fadeIn">
-->

				<!--div class="container session-message">
					<?php #if(($msgs=$this->builtMessages())!==null and $msgs!==array()):?>
					<div class="row">
						<div class="col-lg-12">
							<?php #foreach($msgs as $type => $message):?>
								<div class="alert alert-<?php #echo $type?>">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php #echo $message?>
								</div>
							<?php #endforeach;?>
						</div>
					</div>
					<?php #endif;?>
				</div-->


<section>
        <div class="header-label"></div>
        <div class="container-fluid header">
                <div class="container">
                        <div class="row">
                                        <div class="col-xs-12 col-sm-2 col-nav">
                                                <header>
                                                        <nav>
                                                                <div class="nav-main">
                                                                        <a class="brand-logo link" href="<?php echo $this->createUrl('index')?>">
                                                                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-logo.png" alt="">
                                                                        </a>
                                                                        <div class="user text-center">
                                                                                <div class="user-img">
                                                                                    <?php echo CHtml::image(Yii::app()->session['wepiku_user_img'], "campania",  array("width"=>"76px" ,"height"=>"76px")); ?>
                                                                                </div>
                                                                                <h2><?php echo Yii::app()->session['facebook_nombre']; ?></h2>
                                                                                <?php $image = CHtml::image( Yii::app()->theme->baseUrl."/img/cerrar.png", "salir",  array("width"=>"20px" ,"height"=>"20px")); ?>
                                                                                <h2><?php echo CHtml::link('Salir'.$image, array('site/logout')); ?></h2>
                                                                        </div>
                                                                        <ul class="nav-list">
                                                                                <li>
                                                                                        <a class="nav1" href="<?php echo $this->createUrl('puntos')?>"><span class="icon1"></span>Mis puntos</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a class="nav2" href="<?php echo $this->createUrl('campanias/piks')?>"><span class="icon2"></span>Piks</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a class="nav3" href="<?php echo $this->createUrl('mapa')?>"><span class="icon3"></span>Buscar por zonas</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a class="nav4" href="<?php echo $this->createUrl('marcas')?>"><span class="icon4"></span>Directorio de marcas</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a class="nav5" href="<?php echo $this->createUrl('marcas/crear')?>"><span class="icon5"></span>Crear marca</a>
                                                                                </li>



                                                                                <li>
                                                                                        <a class="nav5" href="<?php echo $this->createUrl('crear')?>"><span class="icon5"></span>Nombre marca</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a class="nav5" href="<?php echo $this->createUrl('como')?>"><span class="icon5"></span>Campañas y encuestas</a>
                                                                                </li>



                                                                                <li>
                                                                                        <a data-toggle="modal" data-target="#modal-stores" class="call-stores"><span class="icon6"></span>Descargar App</a>
                                                                                </li>
                                                                        </ul>
                                                                        <div class="main-social text-center">
                                                                                <a class="social social1" href="https://www.facebook.com/" target="_blank"></a>
                                                                                <a class="social social2" href="https://twitter.com/" target="_blank"></a>
                                                                                <!-- <a class="social social3" href="https://www.youtube.com/" target="_blank"></a> -->
                                                                        </div>
                                                                </div>
                                                        </nav>
                                                </header>
                                        </div>

                                <?php echo $content; ?>

                        </div>
                </div>
        </div>
</section>


<!--
    </div>
</div>
-->
<!-- Modal product -->
<div class="modal modal-item fade" id="modal-product" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
    <div class="modal-scroll">
            <div class="modal-dialog">
                    <div class="modal-content">
                            <div class="modal-header">
                                    <button type="button" class="close close-pro" data-dismiss="modal"></button>
                                    <div class="con-modal-slider">
                                            <div class="discount">
                                                    45%
                                            </div>
                                            <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="8000">
                                                    <ol class="carousel-indicators">
                                                <li data-target=".carousel-fade" data-slide-to="0" class="active"></li>
                                                <li data-target=".carousel-fade" data-slide-to="1"></li>
                                              </ol>
                                              <div class="carousel-inner">
                                                <div class="item active">
                                                 <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/modal-slide1.jpg" alt="">
                                                </div>
                                                <div class="item">
                                                  <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/modal-slide2.jpg" alt="">
                                                </div>
                                              </div>
                                            </div>
                                    </div>
                            </div>
                            <div class="modal-body">
                                    <div class="brand-head">
                                            <a class="brand-logo" href="#">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini5.png" alt="" width="110">
                                            </a>
                                            <?php $image1 = CHtml::image( Yii::app()->theme->baseUrl."/img/share_social.jpg", "share_social",  array("width"=>"20px" ,"height"=>"20px")); ?>
                                            <?php echo CHtml::link($image1, array('site/compartir')); ?>
                                            <h2>VANS CLASSIC VARIOS COLORES</h2>
                                    </div>
                                    <div class="row modal-main-info">
                                            <div class="col-sm-6">
                                                    <h4>Puntos de venta</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            </div>
                                            <div class="col-sm-2 text-center">
                                                    <a class="go-map" href="">
                                                            <span></span>
                                                            <h6>¡Llévame!</h6>
                                                    </a>
                                            </div>
                                            <div class="col-sm-4 text-center">
                                                    <h4>Disponibilidad</h4>
                                                    <h2>3</h2>
                                            </div>
                                    </div>
                                    <div class="row modal-icons row-table">
                                            <div class="col-sm-3 col-cell">
                                                    <span class="icon1"></span>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <span class="icon2"></span>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <span class="icon3"></span>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <span class="icon4"></span>
                                            </div>
                                    </div>
                                    <div class="row modal-icons-info text-center row-table">
                                            <div class="col-sm-3 col-cell">
                                                    <h4>40%</h4>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <h5>Quedan</h5>
                                                    <h4>5 días</h4>
                                                    <h6>del 5 al 10 de octubre</h6>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <h5><a href="" target="_blank">Visitar web</a></h5>
                                            </div>
                                            <div class="col-sm-3 col-cell">
                                                    <h5><a href="#">Ver tiendas</a></h5>
                                            </div>
                                    </div>
                                    <h3>Descripcion</h3>
                                    <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vel enim non nulla mattis elementum quis id tellus. Nam iaculis augue a aliquet pellentesque. Praesent et lectus consectetur, suscipit nunc nec, bibendum ante. Aenean fringilla ipsum id orci viverra, id porta leo interdum. Fusce quis felis eu eros tristique sodales non a mauris.
                                    </p>
                                    <h3>Términos y condiciones</h3>
                                    <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vel enim non nulla mattis elementum quis id tellus. Nam iaculis augue a aliquet pellentesque. Praesent et lectus consectetur, suscipit nunc nec, bibendum ante. Aenean fringilla ipsum id orci viverra, id porta leo interdum. Fusce quis felis eu eros tristique sodales non a mauris.
                                    </p>
                                    <p>
                                            Vestibulum blandit sit amet urna id posuere. Duis ut justo eget lacus sodales tincidunt. Morbi sed molestie diam, posuere egestas ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur sodales mauris dui, nec egestas quam imperdiet ac. Phasellus congue sodales magna eget cursus. Ut ultricies cursus nulla.
                                    </p>
                                    <h3>Comentario</h3>
                                    <div class="comment">
                                            <div class="comment-user">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img1.jpg" alt="" width="44">
                                            </div>
                                            <div class="comment-text">
                                                    <h4>Nombre de Usuario</h4>
                                                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                    <p class="text-right">Hace 2 min</p>
                                            </div>
                                    </div>
                                    <div class="comment">
                                            <div class="comment-user">
                                                    <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img4.jpg" alt="" width="44">
                                            </div>
                                            <div class="comment-text">
                                                    <h4>Nombre de Usuario</h4>
                                                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                                    <p class="text-right">Hace 2 min</p>
                                            </div>
                                    </div>
                                    <div class="comment-area">
                                            <form action="" class="modal-comment clearfix">
                                                    <textarea></textarea>
                                            </form>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                    <p>
                                            Si consideras que este cupón tiene información que
                                            <a href="#">
                                                    reportar
                                            </a>
                                    </p>
                            </div>
                    </div>
            </div>
    </div>
</div>
<!--/ Modal product -->

<!-- Modal stores -->
<div class="modal modal-stores fade" id="modal-stores" role="dialog" aria-labelledby="modal-product" aria-hidden="true">
    <div class="modal-scroll">
            <div class="modal-dialog">
                    <div class="modal-content">
                            <div class="modal-header">
                                    <button type="button" class="close close-app" data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body">
                                    <div class="info-stores">
                                            <h2>DESCARGA <strong>NUESTRA APP</strong></h2>
                                            <a href="">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros</p>
                                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/store1.png" alt="">
                                            </a>
                                            <a href="">
                                                    <p>Lorem ipsum dolor sit amet.</p>
                                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/store2.png" alt="">
                                            </a>
                                    </div>
                                    <div class="app-slider">
                                            <div class="device">
                                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/device.png" alt="">
                                            </div>
                                            <div class="carousel slide carousel-stores" data-ride="carousel" data-interval="4000">
                                              <div class="carousel-inner" role="listbox">
                                                <div class="item active">
                                                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/app-slide1.jpg" alt="">
                                                </div>
                                                <div class="item">
                                                                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/app-slide2.jpg" alt="">
                                                </div>
                                              </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
</div>
<!--/ Modal stores -->


<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/misc.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/application.js" type="text/javascript"></script>


<!--Functions after front-end-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/add/scripts.js" type="text/javascript"></script>

<?php echo $this->builtEndBody()?>
	</body>
</html>
