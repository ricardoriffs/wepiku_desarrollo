<!--					<section>-->


						<div class="col-xs-12 col-sm-7 col-grid col-main section-id section-encuesta">
							<div class="top-search clearfix">
								<form action="#" class="top-form pull-left clearfix">
									<button class="btn-search" type="button"></button>
									<div id="auto-search">
		  							<input class="typeahead" type="text">
									</div>
								</form>
								<div class="dropdown pull-right">
								  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								    <span class="sub-list-icon"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
								  </ul>
								</div>
							</div>


              <div class="info-grl info-form clearfix">
                <div class="row row-info-form">
                  <div class="col-xs-12">
                    <h1>CREAR ENCUESTA</h1>
                  </div>
                  <div class="col-xs-12">
                    <form action="" class="grl-form" method="">

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Nombre de la encuesta</label>
                          <input class="input">
                        </fieldset>
                      </div>

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Cobertura de país y/o ciudades</label>
                          <div class="row row-multi">
                            <select class="multi-select col-xs-4" onchange="multiSelect(this);">
                              <option value="0">Bogotá</option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                              <option value="4">Option 4</option>
                              <option value="5">Option 5</option>
                            </select>
                            <ul class="multi-list col-xs-8">
                              <li onclick="this.parentNode.removeChild(this);">
                                <input type="hidden" name="multi[]" value="0">
                                Bogotá
                              </li>
                            </ul>
                          </div>
                        </fieldset>
                      </div>

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Descripción</label>
                          <textarea class="textarea"></textarea>
                        </fieldset>
                      </div>



											<div class="row">

												<div class="col-xs-12 text-left">
													<h3>TIPO DE PREGUNTA</h3>
												</div>

                        <fieldset class="col-xs-12 col-sm-6 con-field">
                          <label></label>
													<div class="row row-multi">
                            <select class="select col-xs-4">
                              <option value="0"></option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                              <option value="4">Option 4</option>
                              <option value="5">Option 5</option>
                            </select>
                          </div>
                        </fieldset>
                      </div>










                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <div class="label-card">
                            <div class="row">
                              <div class="col-xs-10">
                                  <span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
                                  <p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.</p>
                                </div>
                              <div class="col-xs-2 text-right">
                                <input class="check-active check-active-content" type="checkbox">
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>

                      <div class="row row-item-polls">

                        <div class="col-xs-6">
                          <div class="item-poll">
                            <h3>
                              Cantidad de días en vigencia
                              <span class="item-icon1"></span>
                            </h3>
                            <div class="item-poll-box">
                              <div class="item-poll-box-text clearfix">
                                <div class="table">
                                  <div class="table-cell">
                                    <h2>
                                      Selecciona<br>
                                      hasta <strong>7</strong> días
                                    </h2>
                                  </div>
                                </div>
                                <p>Clic para editar</p>
                              </div>
                              <div class="item-poll-box-edit item-poll-content white-bg">
                                <div class="item-poll-date item-poll-date-content"></div>
                                <div class="item-poll-date-info">
                                  <input id="date-poll-content" value="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xs-6">
                          <div class="item-poll">
                            <h3>
                              Cantidad de encuestas
                              <span class="item-icon2"></span>
                            </h3>
                            <div class="item-poll-box">
                              <div class="item-poll-box-text clearfix">
                                <div class="table">
                                  <div class="table-cell">
                                    <h2>
                                      Se realizarán<br>
                                      hasta <strong>7</strong> encuestas
                                    </h2>
                                  </div>
                                </div>
                                <p>Clic para editar</p>
                              </div>
                              <div class="item-poll-box-edit item-poll-content">
                                <div class="table">
                                  <div class="table-cell">
                                    <div class="cart-counter">
                                      <button class="btn-count-down pull-left" type="button"><span></span></button>
                                      <span class="counter-number">
                                        <span class="number">1</span>
                                      </span>
                                      <button class="btn-count-up pull-right" type="button"><span></span></button>
                                    </div>
                                  </div>
                                </div>
                                <!-- <div class="item-poll-date"></div>
                                <div class="item-poll-date-info">
                                  <input id="date-poll" value="">
                                </div> -->
                              </div>
                            </div>
                            <!-- <div class="item-poll-text">
                              <div class="table">
                                <div class="table-cell">
                                  <h2>
                                    Pueden hacer<br>
                                    hasta <strong>10</strong> Piks
                                  </h2>
                                </div>
                              </div>
                              <p>Clic para editar</p>
                            </div> -->
                          </div>
                        </div>

                      </div>













                    </form>
                  </div>
                </div>
              </div>
						</div>


						<div class="col-xs-12 col-sm-2 col-sub-nav section-plan col-plan">
							<div class="sub-nav active">
								<button class="btn-sub-nav" type="button">
									<span class="btn-arrow"></span>
									<span class="btn-icon"></span>
								</button>
							</div>
							<div class="plan-cost">
								<div class="plan-cost-head">
									<h3>LOREM IPSUM DOLOR</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
									</p>
									<div class="row row-cost-info">
										<div class="col-xs-7 text-left">
											<h5><span class="blue">SALDO ACTIVO</span></h5>
										</div>
										<div class="col-xs-5 text-right">
											<p><span class="blue">000.000,00 $</span></p>
										</div>
									</div>
									<div class="row row-cost-info">
										<div class="col-xs-12 text-left">
											<h5>COSTO CAMPAÑA</h5>
										</div>
										<div class="col-xs-12">
											<div class="row">
												<div class="col-xs-7 text-left">
													<p><strong>Elemento numero 1</strong></p>
												</div>
												<div class="col-xs-5 text-right">
													<p>000.000,00 $</p>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7 text-left">
													<p><strong>Elemento numero 2</strong></p>
												</div>
												<div class="col-xs-5 text-right">
													<p>000.000,00 $</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="plan-cost-total">
									<div class="row">
										<div class="col-xs-4 text-left">
											<h5>TOTAL</h5>
										</div>
										<div class="col-xs-8 text-right">
											<p>000.000,00 $</p>
										</div>
									</div>
								</div>
								<div class="plan-cost-create">
									<button class="btn" type="button">CREAR</button>
								</div>
							</div>
						</div>


<!--					</section>-->
