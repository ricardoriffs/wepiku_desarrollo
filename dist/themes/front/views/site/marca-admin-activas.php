<!--					<section>-->


            <div class="container-fluid brand-page-head section-id">
              <img src="https://scontent-mia.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/11000056_890608347628752_2353305717206992596_n.jpg?oh=3eedb25b5057cbc1f779e217327c5a33&oe=55736EDC" alt="">
            </div>
            <div class="brand-head-fx"></div>


						<div class="col-xs-12 col-sm-7 col-grid col-main grid-section brand-section">
							<div class="top-search clearfix">
								<form action="#" class="top-form pull-left clearfix">
									<button class="btn-search" type="button"></button>
									<div id="auto-search">
		  							<input class="typeahead" type="text">
									</div>
								</form>
								<div class="dropdown pull-right">
								  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								    <span class="sub-list-icon"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
								  </ul>
								</div>
							</div>


              <div class="mg-brand-logo text-center">
                <div class="table">
                  <div class="table-cell">
                    <div class="brand-page-logo">
                      <img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-page.png">
                      <span class="brand-like active">
                        <div class="like-icon"></div>
                      </span>
                    </div>
                    <h1>Nombre de marca</h1>
                  </div>
                </div>
              </div>


              <div class="brand-info">
                <h6 class="text-center">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer vel enim non nulla mattis elementum quis id tellus. Nam iaculis augue a aliquet pellentesque. Praesent et lectus consectetur.
                </h6>
                <div class="row row-table row-brand-links text-center">
                  <div class="col-sm-4 col-cell">
                    <a href="<?php echo $this->createUrl('marca-admin-activas')?>">
                      <p class="tab-text text-left active">Campañas activas</p>
                    </a>
                  </div>
                  <div class="col-sm-4 col-cell">
                    <a href="<?php echo $this->createUrl('marca-admin-pausadas')?>">
                      <p class="tab-text text-left">Campañas pausadas</p>
                    </a>
                  </div>
                  <div class="col-sm-4 col-cell">
                    <a href="<?php echo $this->createUrl('marca-admin-terminadas')?>">
                      <p class="tab-text text-left">Campañas terminadas</p>
                    </a>
                  </div>
                </div>
              </div>


							<div class="wepiku-grid">

                <!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type1">
										<div class="discount" data-toggle="modal" data-target="#modal-product">
											60%
										</div>
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img1.png" alt="">
										<div class="caption">
											<div class="shadow"></div>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini1.png" alt="">
												</a>
											</div>
											<h2 class="clearfix" data-toggle="modal" data-target="#modal-product">Nike Air Force One white / Blue</h2>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type2">
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.jpg" alt="">
										<div class="caption">
											<div class="table">
												<div class="table-cell">
													<div class="row row-table">
														<div class="col-xs-4 col-cell">
															<div class="logo">
																<a href="<?php echo $this->createUrl('marca')?>">
																	<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini2.png" alt="">
																</a>
															</div>
														</div>
														<div class="col-xs-8 col-cell text-right" data-toggle="modal" data-target="#modal-product">
															<h2 class="clearfix">
																Sector 9 Lanza <br>
																Nuevos Equipos <br>
																De Seguridad
															</h2>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type1">
										<div class="discount" data-toggle="modal" data-target="#modal-product">
											2 x 1
										</div>
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img3.jpg" alt="">
										<div class="caption">
											<div class="shadow"></div>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini3.png" alt="">
												</a>
											</div>
											<h2 class="clearfix" data-toggle="modal" data-target="#modal-product">
												<div class="diamont pull-right"></div>
												Colección 2015 Downhill Time Arbor Skateboards
											</h2>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type2">
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img4.jpg" alt="">
										<div class="caption">
											<div class="table">
												<div class="table-cell">
													<div class="row row-table">
														<div class="col-xs-4 col-cell">
															<div class="logo">
																<a href="<?php echo $this->createUrl('marca')?>">
																	<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini4.png" alt="">
																</a>
															</div>
														</div>
														<div class="col-xs-8 col-cell text-right" data-toggle="modal" data-target="#modal-product">
															<h2 class="clearfix">
																Nueva Linea De Levi’s <br>
																Para Skate En Jean
															</h2>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type3">
										<div class="item-type3-bg" style="background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/img/survey-bg.jpg);"></div>
										<div class="caption">
											<p>
												Encuesta
												<div class="icon"></div>
											</p>
											<h2 class="clearfix">
												¿Cómo sueles <br>
												comprar?
											</h2>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-page.png" alt="">
												</a>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type1">
										<div class="discount" data-toggle="modal" data-target="#modal-product">
											20%
										</div>
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img6.jpg" alt="">
										<div class="caption">
											<div class="shadow"></div>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini6.png" alt="">
												</a>
											</div>
											<h2 class="clearfix" data-toggle="modal" data-target="#modal-product">Rayne Slide Series modelos 2015</h2>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type1">
										<div class="discount" data-toggle="modal" data-target="#modal-product">
											45%
										</div>
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img5.jpg" alt="">
										<div class="caption">
											<div class="shadow"></div>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini5.png" alt="">
												</a>
											</div>
											<h2 class="clearfix" data-toggle="modal" data-target="#modal-product">Vans Classic varios colores</h2>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type3">
										<div class="item-type3-bg" style="background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/img/survey-bg.jpg);"></div>
										<div class="caption">
											<p>
												Encuesta
												<div class="icon"></div>
											</p>
											<h2 class="clearfix">
												¿Cómo sueles <br>
												comprar?
											</h2>
											<div class="logo">
												<a href="<?php echo $this->createUrl('marca')?>">
													<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/brand-page.png" alt="">
												</a>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type2">
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img7.jpg" alt="">
										<div class="caption">
											<div class="table">
												<div class="table-cell">
													<div class="row row-table">
														<div class="col-xs-4 col-cell">
															<div class="logo">
																<a href="<?php echo $this->createUrl('marca')?>">
																	<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini7.png" alt="">
																</a>
															</div>
														</div>
														<div class="col-xs-8 col-cell text-right" data-toggle="modal" data-target="#modal-product">
															<h2 class="clearfix">
																Bordados en tela, vuelve el <br>
																look Bohemian Chiq
															</h2>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

								<!-- Item -->
								<div class="wepiku-item">
									<div class="item item-type2">
										<img class="img-main" data-toggle="modal" data-target="#modal-product" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img8.jpg" alt="">
										<div class="caption">
											<div class="table">
												<div class="table-cell">
													<div class="row row-table">
														<div class="col-xs-4 col-cell">
															<div class="logo">
																<a href="<?php echo $this->createUrl('marca')?>">
																	<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo-mini5.png" alt="">
																</a>
															</div>
														</div>
														<div class="col-xs-8 col-cell text-right" data-toggle="modal" data-target="#modal-product">
															<h2 class="clearfix">Vans Classic en cuero</h2>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--/ Item -->

							</div>
						</div>

						<div class="col-xs-12 col-sm-2 col-sub-nav">
							<div class="sub-nav active subnav-hide">
								<button class="btn-sub-nav" type="button">
									<span class="btn-arrow"></span>
									<span class="btn-icon"></span>
								</button>
                <button class="btn-favorite"></button>
							</div>


              <div class="plan-cost card-nav" data-spy="affix" data-offset-top="5" data-offset-bottom="50">
								<div class="plan-cost-head">
                  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-nav.png" alt="" />
									<h4 class="text-center">Lorem ipsum dolor sit amet, consectetur.</h4>
								</div>
								<div class="plan-cost-total">
									<div class="row">
										<div class="col-xs-12 text-center">
											<h6>MONTO RESTANTE</h6>
										</div>
										<div class="col-xs-12 text-center">
											<h4>000.000,00 $</h4>
										</div>
									</div>
								</div>
							</div>


              <div class="year-nav">
                <ul>
                  <li><button class="btn-year active">Reciente</button></li>
                  <li><button class="btn-year">2015</button></li>
                  <li><button class="btn-year">2014</button></li>
                  <li><button class="btn-year">2013</button></li>
                  <li><button class="btn-year">2012</button></li>
                </ul>
              </div>


						</div>


<!--					</section>-->
