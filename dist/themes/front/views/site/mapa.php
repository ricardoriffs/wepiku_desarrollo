<section>
  <div class="col-xs-12 col-sm-7 col-map col-main main-section section-id" data-section="map">
    <div class="top-search top-search-map clearfix">
      <form action="#" class="top-form pull-left clearfix">
        <button class="btn-search" type="button"></button>
        <div id="auto-search">
          <input class="typeahead" type="text">
        </div>
      </form>
      <div class="dropdown pull-right">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
          <span class="sub-list-icon"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
        </ul>
      </div>
    </div>


    <div class="map">
      <div class="bh-sl-container">
        <!-- <div class="bh-sl-form-container">
          <form id="bh-sl-user-location" method="post" action="#">
              <div class="form-input">
                <label for="bh-sl-address">Enter Address or Zip Code:</label>
                <input type="text" id="bh-sl-address" name="bh-sl-address" />
              </div>
              <button id="bh-sl-submit" type="submit">Submit</button>
          </form>
        </div> -->
      </div>
      <!-- <div id="map"></div> -->
      <div id="bh-sl-map" class="bh-sl-map"></div>
    </div>
  </div>


  <!-- <div class="con-section">
    <div class="mg-grl clearfix">
      <div class="row">
        <div class="col-md-6">
          <div class="map">
            <div id="map"></div>
          </div>
        </div>
      </div>
    </div>
  </div> -->


  <div class="col-xs-12 col-sm-2 col-sub-nav col-sub-nav-map">
    <div class="sub-nav active">
      <button class="btn-sub-nav" type="button">
        <span class="btn-arrow"></span>
        <span class="btn-icon-map"></span>
      </button>
      <button class="map-filter" type="button">Todos</button>



      <div class="categories-filter">

      </div>


      <div class="sub-nav-list">
        <div id="map-container" class="bh-sl-map-container">
          <div class="bh-sl-loc-list">
            <ul class="list"></ul>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/stores.js" type="text/javascript"></script>
  <script type="text/javascript">
    // $(function () {
    //     "use strict";
    //     $("#map-container").storeLocator();
    // });
  </script>
</section>
