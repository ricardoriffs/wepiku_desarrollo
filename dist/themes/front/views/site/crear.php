<!--					<section>-->


						<div class="col-xs-12 col-sm-7 col-grid col-main section-id section-new-mark">
							<div class="top-search clearfix">
								<form action="#" class="top-form pull-left clearfix">
									<button class="btn-search" type="button"></button>
									<div id="auto-search">
		  							<input class="typeahead" type="text">
									</div>
								</form>
								<div class="dropdown pull-right">
								  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								    <span class="sub-list-icon"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
								  </ul>
								</div>
							</div>


              <div class="info-grl info-form clearfix">
                <div class="row row-info-form">
                  <!-- <div class="col-xs-12">
                    <h1>CREAR MARCA</h1>
                  </div> -->
                  <div class="col-xs-12">
                    <form action="" class="grl-form" method="">

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Nombre de la marca</label>
                          <input class="input">
                        </fieldset>
                      </div>

											<div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Descripción de la marca</label>
                          <textarea class="textarea"></textarea>
                        </fieldset>
                      </div>

                      <div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Categoría</label>
                          <div class="row row-multi">
                            <select class="multi-select col-xs-4" onchange="multiSelect(this);">
                              <option value="0"></option>
                              <option value="1">Categoría 1</option>
                              <option value="2">Categoría 2</option>
                              <option value="3">Categoría 3</option>
                              <option value="4">Categoría 4</option>
                              <option value="5">Categoría 5</option>
                            </select>
                            <ul class="multi-list col-xs-8">
                              <!-- <li onclick="this.parentNode.removeChild(this);">
                                <input type="hidden" name="multi[]" value="0">
                                Bogotá
                              </li> -->
                            </ul>
                          </div>
                        </fieldset>
                      </div>

											<div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Momentos de consumo asociados</label>
                          <div class="row row-multi">
                            <select class="multi-select col-xs-4" onchange="multiSelect(this);">
                              <option value="0"></option>
                              <option value="1">Momento de consumo 1</option>
                              <option value="2">Momento de consumo 2</option>
                              <option value="3">Momento de consumo 3</option>
                              <option value="4">Momento de consumo 4</option>
                              <option value="5">Momento de consumo 5</option>
                            </select>
                            <ul class="multi-list col-xs-8">
                              <!-- <li onclick="this.parentNode.removeChild(this);">
                                <input type="hidden" name="multi[]" value="0">
                                Bogotá
                              </li> -->
                            </ul>
                          </div>
                        </fieldset>
                      </div>

											<div class="row">
                        <fieldset class="col-xs-12 con-field">
                          <label>Página web</label>
                          <input class="input">
                        </fieldset>
                      </div>

											<div class="row">
                        <fieldset class="col-xs-11 con-field">
                          <label>Página de Facebook</label>
                          <input class="input input-fburl">
                        </fieldset>
												<fieldset class="col-xs-1 con-field con-field-add">
                          <label>&nbsp;</label>
                          <button class="btn btn-add-fburl text-center" type="button"><span></span></button>
                        </fieldset>
                      </div>


											<div class="row row-fburl"></div>


											<div class="row">
												<fieldset class="col-xs-8 con-field">
													<label>Foto de cover</label>
													<input id="file-cover" type="file" multiple="true" class="file-loading">
												</fieldset>
											</div>

											<div class="row">
												<fieldset class="col-xs-8 con-field">
													<label>Foto de perfil</label>
													<input id="file-perfil" type="file" multiple="true" class="file-loading">
												</fieldset>
											</div>

											<div class="con-points">
												<h4 class="points-title">AGREGAR SUCURSAL</h4>
												<div class="row row-point">
													<fieldset class="col-xs-12 col-sm-6 con-field">
	                          <input class="input location" placeholder="Ingresar nombre sucursal">
	                        </fieldset>
													<fieldset class="col-xs-12 col-sm-5 con-field">
														<input class="input position" value="Ubicación" readonly>
														<button type="button" class="btn btn-marker" data-toggle="modal" data-target="#modal-map-new"></button>
	                        </fieldset>
													<fieldset class="col-xs-12 col-sm-1 con-field con-field-add">
														<button type="button" class="btn btn-add-point"><span></span></button>
	                        </fieldset>
													<div class="clearfix"></div>
													<div class="info-location"></div>
												</div>
											</div>

                    </form>
                  </div>
                </div>
              </div>
						</div>


						<div class="col-xs-12 col-sm-2 col-sub-nav section-plan col-plan">
							<div class="sub-nav active">
								<button class="btn-sub-nav" type="button">
									<span class="btn-arrow"></span>
									<span class="btn-icon"></span>
								</button>
							</div>
							<div class="plan-cost">
								<form action="" class="" method="">

									<div class="plan-cost-edit">
										<h4>Email admin</h4>
										<fieldset>
											<input class="input">
										</fieldset>

										<div class="perfil-info">
											<div class="perfil-info-img"><img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img.jpg" alt=""></div>
											<div class="perfil-info-text">Tomás Trujillo</div>
											<button class="perfil-info-close" type="button"></button>
										</div>

										<div class="perfil-info">
											<div class="perfil-info-img"><img class="img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user-img.jpg" alt=""></div>
											<div class="perfil-info-text">Nombre usuario</div>
											<button class="perfil-info-close" type="button"></button>
										</div>

									</div>

									<div class="plan-cost-head">
										<h3>Admin</h3>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus pellentesque.
										</p>
									</div>
									<div class="plan-cost-create save-admin">
										<button class="btn" type="button">GUARDAR</button>
									</div>
								</form>
							</div>
						</div>


<!--					</section>-->


						<!-- Modal -->
						<div class="modal fade modal-map" id="modal-map-new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close close-map" data-dismiss="modal" aria-label="Close">
										</button>
						      </div>
						      <div class="modal-body">
										<div class="row">
											<div class="col-xs-12">
												<div class="map">
													<div id="map"></div>
												</div>
											</div>
										</div>
						      </div>
						    </div>
						  </div>
						</div>
						<!--/ Modal -->


						<!-- Modal -->
						<div class="modal fade modal-map" id="modal-map-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close close-map" data-dismiss="modal" aria-label="Close">
										</button>
						      </div>
						      <div class="modal-body">
										<div class="row">
											<div class="col-xs-12">
												<div class="map">
													<div id="map2"></div>
												</div>
											</div>
										</div>
						      </div>
						    </div>
						  </div>
						</div>
						<!--/ Modal -->


						<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true" type="text/javascript"></script>
						<script type="text/javascript">

							var
									map = new google.maps.Map(document.getElementById("map"), {
											mapTypeControl: false,
											panControl: false,
										  scaleControl: false,
										  streetViewControl: false,
											zoom: 9,
									    zoomControl: true,
									    zoomControlOptions: {
									      style: google.maps.ZoomControlStyle.LARGE,
												position: google.maps.ControlPosition.RIGHT_BOTTOM
									    }
									}),
									map2 = new google.maps.Map(document.getElementById("map2"), {
											mapTypeControl: false,
											panControl: false,
										  scaleControl: false,
										  streetViewControl: false,
											zoom: 9,
									    zoomControl: true,
									    zoomControlOptions: {
									      style: google.maps.ZoomControlStyle.LARGE,
												position: google.maps.ControlPosition.RIGHT_BOTTOM
									    }
									}),
									myMarker = new google.maps.Marker({
											draggable: true,
											icon: "<?php echo Yii::app()->theme->baseUrl; ?>/img/marker-default.png",
											position: new google.maps.LatLng(4.598056, -74.075833)
									}),
									myMarker2 = new google.maps.Marker({
											draggable: true,
											icon: "<?php echo Yii::app()->theme->baseUrl; ?>/img/marker-active.png",
											position: new google.maps.LatLng(4.598056, -74.075833)
									});


							$(document).on("click", ".btn-add-point", function () {
									var
											location = $(".location").val(),
											position = $(".position").val(),
											latlng = new google.maps.LatLng(4.598056, -74.075833);
									myMarker.setPosition(latlng);
									map.setCenter(latlng);
									$(".info-location").append("<div class='row row-new-position'><div class='col-xs-12 col-sm-6'><h5>" + location + "</h5></div><div class='col-xs-12 col-sm-4'><h5 class='position-reg'>" + position + "</h5></div><div class='col-xs-6 col-sm-1'><button class='btn btn-edit-position' type='button' data-toggle='modal' data-target='#modal-map-edit'></button></div><div class='col-xs-6 col-sm-1'><button class='btn btn-remove' type='button'></button></div><span class='row-br'></span></div>");
									$(".location").val("");
									$(".position").val("Ubicación");
							});


							$(document).on("click", ".btn-edit-position", function () {
									$(this).parent().siblings().find(".position-reg").text("Ubicación");
									$(this).parent().siblings().find(".position-reg").addClass("position-edit");
									setTimeout(function () {
										center = map2.getCenter();
										google.maps.event.trigger(map2, "resize");
										map2.setCenter(center);
									}, 400);
							});


							google.maps.event.addListener(myMarker, "dragend", function(evt){
									document.querySelector(".position").value = evt.latLng.lat().toFixed(6) + ", " + evt.latLng.lng().toFixed(6);
							});


							google.maps.event.addListener(myMarker2, "dragend", function(evt){
									document.querySelector(".position-edit").innerHTML = evt.latLng.lat().toFixed(6) + ', ' + evt.latLng.lng().toFixed(6);
							});


							map.setCenter(myMarker.position);
							map2.setCenter(myMarker2.position);
							myMarker.setMap(map);
							myMarker2.setMap(map2);


							$(document).on("click", ".btn-marker", function () {
									"use strict";
									console.log("new");
									setTimeout(function () {
										console.log("time");
										var center = map.getCenter();
										google.maps.event.trigger(map, "resize");
										map.setCenter(center);
									}, 400);
							});


							$(document).on("click", ".close-map", function () {
									"use strict";
									$(".position-reg").removeClass("position-edit");
							});


							$(document).on("click", ".modal-backdrop", function () {
									"use strict";
									$(".position-reg").removeClass("position-edit");
							});


							$("html, body").on("keyup", function (e) {
					        if (e.keyCode === 27) {
											$(".position-reg").removeClass("position-edit");
					        }
					    });


						</script>
