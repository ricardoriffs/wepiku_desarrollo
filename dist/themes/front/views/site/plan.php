<!--					<section>-->


						<div class="col-xs-12 col-sm-7 col-grid col-main section-id section-plan">
							<div class="top-search clearfix">
								<form action="#" class="top-form pull-left clearfix">
									<button class="btn-search" type="button"></button>
									<div id="auto-search">
		  							<input class="typeahead" type="text">
									</div>
								</form>
								<div class="dropdown pull-right">
								  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								    <span class="sub-list-icon"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
								  </ul>
								</div>
							</div>


              <div class="info-grl info-form clearfix">
                <div class="row row-info-form">
                  <div class="col-xs-12">
                    <h1>CREAR UNA CAMPAÑA</h1>
                  </div>
                  <div class="col-xs-12">
                    <form action="" class="grl-form" method="">

                      <fieldset class="con-field">
                        <label>Nombre campaña</label>
                        <input class="input">
                      </fieldset>

                      <div class="form-tabs" role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                            <a href="#content" aria-controls="content" role="tab" data-toggle="tab">CONTENIDO</a>
                          </li>
                          <li role="presentation">
                            <a href="#promotion" aria-controls="promotion" role="tab" data-toggle="tab">PROMOCIÓN</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                          <div role="tabpanel" class="tab-pane active" id="content">

														<div class="row">
	                            <fieldset class="col-xs-12 con-field">
	                              <label>Descripción</label>
	                              <textarea class="textarea"></textarea>
	                            </fieldset>
														</div>

														<div class="row">
	                            <fieldset class="col-xs-12 con-field con-field-video">
	                              <label>Videos o imágenes</label>
																	<div class="row row-videos">
																		<div class="col-xs-12">
																			<ul class="list-videos clearfix"></ul>
																		</div>
																	</div>
	                            		<input class="input input-video video-content" placeholder="Ingresa URL">
																	<button class="btn btn-primary btn-add-video-content" type="button">Subir video</button>
	                            </fieldset>
														</div>

														<div class="row">
	                            <fieldset class="col-xs-8 con-field">
	                              <input id="file-content" type="file" multiple="true" class="file-loading">
	                            </fieldset>

															<fieldset class="col-xs-4 con-field">
																<select class="select">
																	<option value="0">Seleccione el tamaño...</option>
																	<option value="1">&nbsp; &bull; 320px por 320px</option>
																	<option value="2">&nbsp; &bull; 640px por 640px</option>
																	<option value="3">&nbsp; &bull; 720px por 720px</option>
																</select>
	                            </fieldset>

														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
	                              <label>Cobertura de país y/o ciudades</label>
																<div class="row row-multi">
		                              <select class="multi-select col-xs-4" onchange="multiSelect(this);">
		                              	<option value="0">Bogotá</option>
		                              	<option value="1">Option 1</option>
		                              	<option value="2">Option 2</option>
		                              	<option value="3">Option 3</option>
		                              	<option value="4">Option 4</option>
		                              	<option value="5">Option 5</option>
		                              </select>
																	<ul class="multi-list col-xs-8">
																		<li onclick="this.parentNode.removeChild(this);">
																		  <input type="hidden" name="multi[]" value="0">
																			Bogotá
																		</li>
																	</ul>
																</div>
	                            </fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
	                              <label>Sucursales</label>
																<div class="row row-multi">
		                              <select class="multi-select col-xs-4" onchange="multiSelect(this);">
		                              	<option value="0">Bogotá</option>
		                              	<option value="1">Option 1</option>
		                              	<option value="2">Option 2</option>
		                              	<option value="3">Option 3</option>
		                              	<option value="4">Option 4</option>
		                              	<option value="5">Option 5</option>
		                              </select>
																	<ul class="multi-list col-xs-8">
																		<li onclick="this.parentNode.removeChild(this);">
																		  <input type="hidden" name="multi[]" value="0">
																			Bogotá
																		</li>
																	</ul>
																</div>
	                            </fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-4 con-field text-left">
																<button class="btn btn-primary btn-add-poll" type="button">
																	INCLUIR ENCUESTA
																	<span></span>
																</button>
															</fieldset>
															<fieldset class="col-xs-8 con-field">
																<p class="p-small text-left">
																	<strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.
																</p>
															</fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
																<div class="label-card label-diamont">
																	<div class="row">
																		<div class="col-xs-10">
																				<span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamont-small.png" alt=""></span>
																				<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.</p>
																			</div>
																		<div class="col-xs-2 text-right">
																			<input class="check-active" type="checkbox">
																		</div>
																	</div>
																</div>
															</fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
																<div class="label-card">
																	<div class="row">
																		<div class="col-xs-10">
																				<span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
																				<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.</p>
																			</div>
																		<div class="col-xs-2 text-right">
																			<input class="check-active check-active-content" type="checkbox">
																		</div>
																	</div>
																</div>
															</fieldset>
														</div>

														<div class="row row-item-polls">

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Días Vigencia
																		<span class="item-icon1"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Selecciona<br>
																						hasta <strong>7</strong> días
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content white-bg">
																			<div class="item-poll-date item-poll-date-content"></div>
																			<div class="item-poll-date-info">
																				<input id="date-poll-content" value="">
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Número de piks
																		<span class="item-icon2"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer<br>
																						hasta <strong>10</strong> Piks
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																			<!-- <div class="item-poll-date"></div>
																			<div class="item-poll-date-info">
																				<input id="date-poll" value="">
																			</div> -->
																		</div>
																	</div>
																	<!-- <div class="item-poll-text">
																		<div class="table">
																			<div class="table-cell">
																				<h2>
																					Pueden hacer<br>
																					hasta <strong>10</strong> Piks
																				</h2>
																			</div>
																		</div>
																		<p>Clic para editar</p>
																	</div> -->
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Recomendaciones
																		<span class="item-icon3"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer hasta<br>
																						<strong>10</strong> recomendaciones
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>

														<div class="row row-item-polls">

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Visualizaciones
																		<span class="item-icon3"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer hasta<br>
																						<strong>10</strong> Visualizaciones
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Impresiones
																		<span class="item-icon3"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer hasta<br>
																						<strong>10</strong> Impresiones
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Ingresos al detalle
																		<span class="item-icon3"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer hasta<br>
																						<strong>10</strong> Ingresos
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-content">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
                          </div>


                          <div role="tabpanel" class="tab-pane" id="promotion">

														<div class="row">
	                            <fieldset class="col-xs-12 con-field">
	                              <label>Descripción</label>
	                              <textarea class="textarea"></textarea>
	                            </fieldset>
														</div>

														<div class="row">
	                            <fieldset class="col-xs-12 con-field">
	                              <label>Ingresar oferta del producto</label>
	                              <input class="input">
	                            </fieldset>
														</div>

														<div class="row">
	                            <fieldset class="col-xs-12 con-field con-field-video">
	                              <label>Videos o imágenes</label>
																	<div class="row row-videos">
																		<div class="col-xs-12">
																			<ul class="list-videos clearfix"></ul>
																		</div>
																	</div>
	                            		<input class="input input-video video-promotion" placeholder="Ingresa URL">
																	<button class="btn btn-primary btn-add-video-promotion" type="button">Subir video</button>
	                            </fieldset>
														</div>

														<div class="row">
	                            <fieldset class="col-xs-8 con-field">
																<input id="file-promotion" type="file" multiple="true" class="file-loading">
	                            </fieldset>

															<fieldset class="col-xs-4 con-field">
																<select class="select">
																	<option value="0">Seleccione el tamaño...</option>
																	<option value="1">&nbsp; &bull; 320px por 320px</option>
																	<option value="2">&nbsp; &bull; 640px por 640px</option>
																	<option value="3">&nbsp; &bull; 720px por 720px</option>
																</select>
	                            </fieldset>

														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
	                              <label>Cobertura de país y/o ciudades</label>
																<div class="row row-multi">
		                              <select class="multi-select col-xs-4" onchange="multiSelect(this);">
		                              	<option value="0">Bogotá</option>
		                              	<option value="1">Option 1</option>
		                              	<option value="2">Option 2</option>
		                              	<option value="3">Option 3</option>
		                              	<option value="4">Option 4</option>
		                              	<option value="5">Option 5</option>
		                              </select>
																	<ul class="multi-list col-xs-8">
																		<li onclick="this.parentNode.removeChild(this);">
																		  <input type="hidden" name="multi[]" value="0">
																			Bogotá
																		</li>
																	</ul>
																</div>
	                            </fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
	                              <label>Sucursales</label>
																<div class="row row-multi">
		                              <select class="multi-select col-xs-4" onchange="multiSelect(this);">
		                              	<option value="0">Bogotá</option>
		                              	<option value="1">Option 1</option>
		                              	<option value="2">Option 2</option>
		                              	<option value="3">Option 3</option>
		                              	<option value="4">Option 4</option>
		                              	<option value="5">Option 5</option>
		                              </select>
																	<ul class="multi-list col-xs-8">
																		<li onclick="this.parentNode.removeChild(this);">
																		  <input type="hidden" name="multi[]" value="0">
																			Bogotá
																		</li>
																	</ul>
																</div>
	                            </fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-4 con-field text-left">
																<button class="btn btn-primary btn-add-poll" type="button">
																	INCLUIR ENCUESTA
																	<span></span>
																</button>
															</fieldset>
															<fieldset class="col-xs-8 con-field">
																<p class="p-small text-left">
																	<strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.
																</p>
															</fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
																<div class="label-card label-diamont">
																	<div class="row">
																		<div class="col-xs-10">
																				<span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/diamont-small.png" alt=""></span>
																				<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.</p>
																			</div>
																		<div class="col-xs-2 text-right">
																			<input class="check-active" type="checkbox">
																		</div>
																	</div>
																</div>
															</fieldset>
														</div>

														<div class="row">
															<fieldset class="col-xs-12 con-field">
																<div class="label-card">
																	<div class="row">
																		<div class="col-xs-10">
																				<span><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/card-small.png" alt=""></span>
																				<p><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Maecenas ac eros purus. Pellentesque habitant morbi et netus et malesuada fames ac turpis egestas.</p>
																			</div>
																		<div class="col-xs-2 text-right">
																			<input class="check-active check-active-promotion" type="checkbox">
																		</div>
																	</div>
																</div>
															</fieldset>
														</div>

														<div class="row row-item-polls">
															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Días Vigencia
																		<span class="item-icon1"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Selecciona<br>
																						hasta <strong>7</strong> días
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-promotion white-bg">
																			<div class="item-poll-date item-poll-date-promotion"></div>
																			<div class="item-poll-date-info">
																				<input id="date-poll-promotion" value="">
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Número de piks
																		<span class="item-icon2"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer<br>
																						hasta <strong>10</strong> Piks
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-promotion">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="col-xs-4">
																<div class="item-poll">
																	<h3>
																		Recomendaciones
																		<span class="item-icon3"></span>
																	</h3>
																	<div class="item-poll-box">
																		<div class="item-poll-box-text clearfix">
																			<div class="table">
																				<div class="table-cell">
																					<h2>
																						Pueden hacer hasta<br>
																						<strong>10</strong> recomendaciones
																					</h2>
																				</div>
																			</div>
																			<p>Clic para editar</p>
																		</div>
																		<div class="item-poll-box-edit item-poll-promotion">
																			<div class="table">
																				<div class="table-cell">
																					<div class="cart-counter">
																						<button class="btn-count-down pull-left" type="button"><span></span></button>
																						<span class="counter-number">
																							<span class="number">1</span>
																						</span>
																						<button class="btn-count-up pull-right" type="button"><span></span></button>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
													</div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
						</div>


						<div class="col-xs-12 col-sm-2 col-sub-nav">
							<div class="sub-nav active">
								<button class="btn-sub-nav" type="button">
									<span class="btn-arrow"></span>
									<span class="btn-icon"></span>
								</button>
							</div>
							<div class="plan-cost">
								<div class="plan-cost-head">
									<h3>LOREM IPSUM DOLOR</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac eros purus.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
									</p>
									<div class="row row-cost-info">
										<div class="col-xs-7 text-left">
											<h5><span class="blue">SALDO ACTIVO</span></h5>
										</div>
										<div class="col-xs-5 text-right">
											<p><span class="blue">000.000,00 $</span></p>
										</div>
									</div>
									<div class="row row-cost-info">
										<div class="col-xs-12 text-left">
											<h5>COSTO CAMPAÑA</h5>
										</div>
										<div class="col-xs-12">
											<div class="row">
												<div class="col-xs-7 text-left">
													<p><strong>Elemento numero 1</strong></p>
												</div>
												<div class="col-xs-5 text-right">
													<p>000.000,00 $</p>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-7 text-left">
													<p><strong>Elemento numero 2</strong></p>
												</div>
												<div class="col-xs-5 text-right">
													<p>000.000,00 $</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="plan-cost-total">
									<div class="row">
										<div class="col-xs-4 text-left">
											<h5>TOTAL</h5>
										</div>
										<div class="col-xs-8 text-right">
											<p>000.000,00 $</p>
										</div>
									</div>
								</div>
								<div class="plan-cost-create">
									<button class="btn" type="button">CREAR</button>
								</div>
							</div>
						</div>


<!--					</section>-->
