<?php

class MarcasController extends Controller{
    
    public function actionMarca(){
        
    $this->layout = 'landing';               
    if(isset($_GET['theme']))
        Yii::app()->theme=$_GET['theme'];            

    $url = 'http://192.237.180.31/wepiku/marca/api_marcacategoria';
    $access_key = 'a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
    
    $curl = curl_init($url);
    // Optional header
    curl_setopt($curl, CURLOPT_HTTPHEADER,  array( 'Authorization: Bearer ' . $access_key ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    // get the result and parse to JSON
    $jsondecode = CJSON::decode($result, true);
    $marcas_datos =$jsondecode['data']['marcas'];
   		
    $this->render('marca', array('marcas_datos' => $marcas_datos));
    
    }
    
    public function actionCrear(){
     
            $json = 
            '{
                    "nombre": "Inbytecore",
                    "descripcion": "Desarrollo de software a la medida",
                    "pagina_web": "www.ibctecnologia.com",
                    "facebook_links":{
                        "0":"https://www.facebook.com/ibctecnologia?ref=aymt_homepage_panel",
                        "1":"https://www.facebook.com/pumalifepy?fref=ts",
                        "2":"https://www.facebook.com/puma.peru.1?fref=ts"
                    },
                    "sucursales":[
                        {
                         "nombre":"Sede Alfa",
                         "lat":"4.00000",
                         "lng":"-72.000",
                         "direccion":"Carrera 14 # 54-31, Bogotá, Cundinamarca, Colombia",
                         "inactiva":0
                        },
                        {
                         "nombre":"Sede Beta",
                         "lat":"4.6764972",
                         "lng":"-74.056667",
                         "direccion":"Carrera 14 # 54-31, Bogotá, Cundinamarca, Colombia",
                         "inactiva":0
                        }
                    ],
                    "categorias":{
                        "0":8
                    },
                    "momentos":{
                    },
                    "administradores":{
                    }
            }';                    
                    
            $url = 'http://192.237.180.31/wepiku/marca/api_marcamarca/create';
            
            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            //$wepiku_token = 'a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
            $token = 'Bearer '.$wepiku_token;
            $response = Yii::app()->curl->setHeaders(array('Authorization' => $token, 'Content-Type' => 'application/json'))->post($url, $json);                 
          
            $jsondecode = CJSON::decode($response, true);
            $responseData =(array)$jsondecode;
             print_r($responseData);          
    }


    public static function actionDetalleMarcaBackend($marca_id){                   

        $url = 'http://192.237.180.31/wepiku/marca/api_marcamarca/obtener_detalle?marca_id='.$marca_id;

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     

        $jsondecode = CJSON::decode($response);
        $responseData =(array)$jsondecode;    
        
        return $responseData['data']['nombre'];
    }
}

