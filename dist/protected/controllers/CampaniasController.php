<?php

class CampaniasController extends Controller{
    
    public $layout='main';
    
    public function missingAction($actionID)
    {
            // Yii::app()->user->setFlash('warning','This is a test');
            // Yii::app()->user->setFlash('danger','This is a test');
            // Yii::app()->user->setFlash('success','This is a test');

            if($actionID=='' or $actionID=='main.php'):
                    $actionID='index';
                    $this->layout = 'landing';
            endif;

            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            $this->render(strtr($actionID,array('.php'=>'')));
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
    }    
    
    public function actionDetalle(){
        
        $this->render('detalle');
    }
    
    /**Lista de Promociones que el usuario ha decidido seguir con piks**/    
    
    public function actionPiks(){
        
        if(isset($_GET['theme']))
            Yii::app()->theme=$_GET['theme'];                      

        $url = 'http://192.237.180.31/wepiku/campania/api_campanias/getpiks';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     

        $jsondecode = CJSON::decode($response);
        $responseData =(array)$jsondecode;
        
        //HotdealsController::actionImpresionesXUsuarioBackend($responseData); //habilitar cuando se suba al servidor
        
        function ordenar($a, $b ) {
            return strtotime($a['fecha_finalizacion']) - strtotime($b['fecha_finalizacion']);
        }        
        
        usort($responseData['data'], 'ordenar');
        
        //foreach($responseData['data'] as $data_hotdeals_json):
        //    echo '<br/>fecha: '.$data_hotdeals_json['fecha_finalizacion'].'campaña: '.$data_hotdeals_json['nombre'];
        //endforeach;
       
        $this->render('piks', array('data_hotdeals_json' => $responseData));        
    }
}