<?php
require_once( dirname(__FILE__) . '/../controllers/MarcasController.php');

class HotdealsController extends Controller{
    
    public $layout='main';
    
    public function missingAction($actionID)
    {
            // Yii::app()->user->setFlash('warning','This is a test');
            // Yii::app()->user->setFlash('danger','This is a test');
            // Yii::app()->user->setFlash('success','This is a test');

            if($actionID=='' or $actionID=='main.php'):
                    $actionID='index';
                    $this->layout = 'landing';
            endif;

            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            $this->render(strtr($actionID,array('.php'=>'')));
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            $this->render('error', $error);
            }
    }
    
     
    public function actionIndex(){
        
        $response_marca = array(); 
        
        //$this->layout = 'landing';               
        if(isset($_GET['theme']))
            Yii::app()->theme=$_GET['theme'];                      

        //$url = 'http://192.237.180.31/wepiku/campania/api_campanias?city_id=11001';
        $url = 'http://192.237.180.31/wepiku/campania/api_campanias?lat=0&lng=0&marca_id=0';

        $wepiku_token = Yii::app()->session['wepiku_access_token'];
        $token = 'Bearer '.$wepiku_token;
        $auth = array('Authorization: '.$token);
        $response  = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->get($url);                     

        $jsondecode = CJSON::decode($response);
        $response_hotdeals =(array)$jsondecode;
        //fecha_creacion ---ordenar por---
        //costo_multimedia_id ---ordenar por---
        
        //HotdealsController::actionImpresionesXUsuarioBackend($responseData); //habilitar cuando se suba al servidor
        $response_newsfeed = HotdealsController::actionNewsFeedBackend();
        
        foreach ($response_newsfeed['data'] as $newfeed):
            $existe_marca = false;  
        
            foreach ($response_marca as $marca):
                if($marca == $newfeed['campania']['marca_id']):
                    $existe_marca = true;
                endif;
            endforeach;
            if(!$existe_marca):
                $response_marca[$newfeed['campania']['marca_id']] = MarcasController::actionDetalleMarcaBackend($newfeed['campania']['marca_id']);                  
            endif;                          
         endforeach;

//        foreach ($response_marca as $key => $marca):
//            echo 'marca'.$key.': '.$marca.'<br/>';
//        endforeach;
        
        $this->render('index', array('data_hotdeals_json' => $response_hotdeals, 'data_newsfeed_json' => $response_newsfeed, 'data_marca_json' => $response_marca));
    }
    
    public function actionNewsFeedBackend(){

            $post_data = array();            
            
            $url = 'http://192.237.180.31/wepiku/usuario/api_usuarioaccion/newfeeds';

            //$wepiku_token = Yii::app()->session['wepiku_access_token'];
            $wepiku_token = 'a8dYbYn8w4mRc6x6pAy65Tg258fTnSoQ3SgMiSgT';
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);            
            $response = Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);   
            
            $jsondecode = CJSON::decode($response);
            $responseData =(array)$jsondecode;
            return $responseData;                 
    }
    
    public function actionImpresionesXUsuarioBackend($data_hotdeals_json){
        
        foreach ($data_hotdeals_json['data'] as $campania):
            $post_data = array(
                'campanias_ids' => $campania['id'],
                  );
            
            $url = 'http://192.237.180.31/wepiku/usuario/api_usuarioaccion/accion_dibujar';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);            
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);   
            
            //$jsondecode = CJSON::decode($response);
            //$responseData =(array)$jsondecode;
            //print_r($responseData['data']);
            //echo 'campania_id: '.$campania['id'];
        endforeach;    
    }
}