<?php
class SiteController extends Controller
{
    
	public $layout='main';
	public function missingAction($actionID)
	{
		// Yii::app()->user->setFlash('warning','This is a test');
		// Yii::app()->user->setFlash('danger','This is a test');
		// Yii::app()->user->setFlash('success','This is a test');

		if($actionID=='' or $actionID=='main.php'):
			$actionID='login';
			$this->layout = 'landing';
		endif;

		if(isset($_GET['theme']))
			Yii::app()->theme=$_GET['theme'];
		$this->render(strtr($actionID,array('.php'=>'')));
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if(isset($_GET['theme']))
			Yii::app()->theme=$_GET['theme'];
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}      
         
        public function actionLogin(){
            
	    $this->layout = 'landing';	            
            if(isset($_GET['theme']))
                    Yii::app()->theme=$_GET['theme'];	            
            if (!isset($_GET['provider']))
               {
                   $this->render('login');
               } else {
                    $provider = $_GET['provider'];
                    $datos_usuario_json = array();
                        
                    try
                    {
                        
                        //Yii::import('components.HybridAuthIdentity'); //ext.components.HybridAuthIdentity
                        $haComp = new HybridAuthIdentity();
                        if (!$haComp->validateProviderName($provider))
                            throw new CHttpException ('500', 'Acción Inválida. Por favor intente nuevamente.');
                            
                        $haComp->adapter = $haComp->hybridAuth->authenticate($provider);
                        $haComp->userProfile = $haComp->adapter->getUserProfile();
                        
                        if(isset($haComp->userProfile)){
                            ///
                            $token = $haComp->adapter->getAccessToken();
                            Yii::app()->session['facebook_nombre'] = $haComp->userProfile->displayName;
                            //$haComp->userProfile;
                            $datos_usuario_json = SiteController::actionLoginBackend($haComp->userProfile->identifier, $token["access_token"]);                          
                             
                            if(isset($datos_usuario_json)):
                                SiteController::actionIngresoDiarioBackend();
                            endif;
                            
                            //print_r($datos_usuario_json);
                            //echo 'id: '.$haComp->userProfile->identifier; 
                            //echo 'token: '.$token["access_token"];
                            //$haComp->login();
                            //$this->render('index', array('datos_usuario_json' => $datos_usuario_json));                            
                            $this->redirect($this->createUrl('/hotdeals/index'));  //redirect to the user logged in section..                                                         
                        }                        
                    }
                    catch (Exception $e)
                    {
                        //process error message as required or as mentioned in the HybridAuth 'Simple Sign-in script' documentation
                        $this->render('login', array('error' => $e->getMessage()));
                        //throw new CHttpException('500','The requested page does not exist.'.$e->getMessage());
                        //$this->redirect($this->createUrl('/site/login'));
                        
                    } 
               }       
        }  
        
        public function actionSocialLogin()
        {
            //Yii::import('components.HybridAuthIdentity'); //ext.components.HybridAuthIdentity
            $path = Yii::getPathOfAlias('ext.HybridAuth');
            require_once $path . '/hybridauth-' . HybridAuthIdentity::VERSION . '/hybridauth/index.php';

        }        
         
        public function actionLogout(){
            
            //logout
            //unset(Yii::app()->session['facebook_token']);
            //Yii::app()->session->remove('facebook_token');
            Yii::app()->session->clear();
            Yii::app()->session->destroy();
            
            $this->redirect(array('login'));
        }
        
        public function actionLoginBackend($facebook_id, $facebook_token){
           
            //$header = array('Content-Type: multipart/form-data');
            
            $post_data = array(
                'client_id' => 'ios',
                'client_secret' => 'TjBtAmVtNqIj83SvHi8hK0I32k4e27S7',
                'facebook_token' => $facebook_token,                
                'facebook_id' => $facebook_id
                  );
            
            $url = 'http://192.237.180.31/wepiku/usuario/api_usuarios/login';

            $response = Yii::app()->curl->post($url, $post_data, array(), true);                          
          
            $jsondecode = CJSON::decode($response);
            Yii::app()->session['wepiku_access_token'] =$jsondecode['data']['wepiku_token']['access_token'];
            Yii::app()->session['wepiku_user_img'] =$jsondecode['data']['wepiku_user']['img'];
            $responseData =(array)$jsondecode;
            return $responseData;            
        }
        
        public function actionIngresoDiarioBackend(){
            
            $post_data = array();            
            
            $url = 'http://192.237.180.31/wepiku/usuario/api_usuarioaccion/accion_ingreso_diario';

            $wepiku_token = Yii::app()->session['wepiku_access_token'];
            $token = 'Bearer '.$wepiku_token;
            $auth = array('Authorization: '.$token);            
            Yii::app()->curl->setOption(CURLOPT_HTTPHEADER, $auth)->post($url, $post_data);          
        }


        public function compartir(){
            
        }
}
