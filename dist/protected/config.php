<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__),
    'theme'=>'front',
    // preloading 'log' component
    'preload' => array('log'),
    // 'defaultController' => 'admin',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    // application components
    'components' => array(
        'user'=>array(
            // this is actually the default value
            'enableAutoLogin' => false,
            'loginUrl'=>array('site/login'),
        ),
        'curl' => array(
            'class' => 'ext.curl.Curl',
            'options' => array(/* additional curl options */),
        ),    
        // Handling Session
        'session' => array (
            'autoStart' => 'true',
            //'timeout' => 600,
            //'class' => 'CHttpSession',
            /*'sessionName' => 'Site Access',
            'cookieMode' => 'only',
            'savePath' => '/path/to/new/directory', */           
        ),        
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            //'urlSuffix' => '.html',
            'rules' => array(
              'deploy/themes/<theme:\w+>/views/layouts/<page:\w+\.php>' => '<controller>/<page>',
              'deploy/themes/<theme:\w+>/views/<controller:\w+>/<page:\w+\.php>' => '<controller>/<page>',
              '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
              '<controller:\w+>/<id:\d+>' => '<controller>/view',
              '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
              '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        // uncomment the following to use a MySQL database
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => '/site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                // array(
                //     'class' => 'CFileLogRoute',
                //     'levels' => 'error, warning',
                // ),
            ),
        ),
        'clientScript'=>array(
            'packages'=>array(
                'jquery-front'=>array(
                    'baseUrl'=>'http://ajax.googleapis.com/ajax/libs/jquery/',
                    'js'=>array('2.1.3/jquery.min.js'),
                ),
                'jquery-back'=>array(
                    'baseUrl'=>'http://ajax.googleapis.com/ajax/libs/jquery/',
                    'js'=>array('1.8.3/jquery.min.js'),
                )
            ),
        ),
    ),
);
